---

image: debian:bookworm-slim

.apt-template: &apt-template
  variables:
    DEBIAN_FRONTEND: noninteractive
    LANG: C.UTF-8
  before_script:
    - echo Etc/UTC > /etc/timezone
    - echo 'APT::Install-Recommends "0";'
           'APT::Install-Suggests "0";'
           'APT::Get::Assume-Yes "true";'
           'Acquire::Retries "20";'
           'Dpkg::Use-Pty "0";'
           'quiet "1";'
        >> /etc/apt/apt.conf.d/99gitlab
    # Ubuntu and other distros often lack https:// support
    - grep Debian /etc/issue.net
        && { find /etc/apt/sources.list* -type f | xargs sed -i s,http:,https:, ; }
    - echo 'Acquire::https::Verify-Peer "false";' > /etc/apt/apt.conf.d/99nocacertificates
    - apt-get update
    - apt-get install ca-certificates
    - rm /etc/apt/apt.conf.d/99nocacertificates
    - apt-get dist-upgrade


lint:
  <<: *apt-template
  image: registry.gitlab.com/fdroid/fdroidserver:buildserver-bookworm
  script:
    - rm -rf fdroidserver
    - mkdir fdroidserver
    - git ls-remote https://gitlab.com/fdroid/fdroidserver.git master
    - curl --silent https://gitlab.com/fdroid/fdroidserver/-/archive/master/fdroidserver-master.tar.gz
        | tar -xz --directory=fdroidserver --strip-components=1
    - export PATH="$PWD/fdroidserver:$PATH"

    # if this is a merge request fork, then only check relevant apps
    - if [ "$CI_PROJECT_NAMESPACE" != "fdroid" ]; then
        git fetch https://gitlab.com/guardianproject/fdroid-metadata.git;
        test -d build || mkdir build;
        files=`git diff --name-only --diff-filter=d FETCH_HEAD...HEAD`;
        for f in $files; do
           appid=`echo $f | sed -n -e 's,^metadata/\([^/][^/]*\)\.yml,\1,p'`;
           if [ -n "$appid" ]; then
             export CHANGED="$CHANGED $appid";
             testcmd="git -C build clone `sed -En 's,^Repo\W +(https?://),\1u:p@,p' $f`";
           fi;
        done;
        set -x;
        apt-get install python3-colorama;
        ./tools/check-git-repo-availability.py $files;
        ./tools/audit-gradle.py $CHANGED;
        set +x;
      fi
    - fdroid lint -f $CHANGED || true  # too pedantic for us
    - export EXITVALUE=0
    - function set_error() { export EXITVALUE=1; printf "\x1b[31mERROR `history|tail -2|head -1|cut -b 6-500`\x1b[0m\n"; }
    - chmod 0600 config.yml
    - fdroid readmeta || set_error
    - apt-get -qy install --no-install-recommends exiftool git python3-yaml yamllint
    - find metadata/ -name '*.jp*g' -o -name '*.png' | xargs exiftool -all=
    - echo "these images have EXIF that must be stripped:"
    - git --no-pager diff --stat
    - git --no-pager diff --name-only --exit-code || set_error
    - ./tools/check-localized-metadata.py || set_error
    - ./tools/check-keyalias-collision.py || set_error
    - ./tools/check-metadata-summary-whitespace.py || set_error
    - for f in metadata/*.yml; do yamllint $f || set_error; done
    - exit $EXITVALUE

shellcheck:
  only:
    changes:
      - .gitlab-ci.yml
      - download-tor-browser-updates.sh
      - update
  <<: *apt-template
  script:
    - apt-get install bash shellcheck
    - bash -n update *.sh */*.sh etc-cron.daily-script
    - shellcheck --color --severity=warning update *.sh */*.sh etc-cron.daily-script
